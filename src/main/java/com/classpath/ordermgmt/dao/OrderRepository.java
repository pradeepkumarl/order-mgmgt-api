package com.classpath.ordermgmt.dao;

import com.classpath.ordermgmt.model.Customer;
import com.classpath.ordermgmt.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Set;

@Repository
public interface OrderRepository  extends JpaRepository<Order, Long> {

    Set<Order> findOrdersByCustomer(Customer customer);
}