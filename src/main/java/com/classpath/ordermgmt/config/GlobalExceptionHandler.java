package com.classpath.ordermgmt.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrderId(Exception exception){
        return ResponseEntity.status(BAD_REQUEST).body(new Error(20, exception.getMessage()));
    }
}

@AllArgsConstructor
@Getter
@Setter
class Error {
    private int code;
    private String message;
}