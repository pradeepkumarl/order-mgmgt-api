package com.classpath.ordermgmt.controller;

import com.classpath.ordermgmt.model.Order;
import com.classpath.ordermgmt.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;


    @PostMapping
    @ResponseStatus(CREATED)
    public Order save(@RequestBody @Valid Order order){
        return this.orderService.save(order);
    }

    @GetMapping
    public Set<Order> fetchAll(){
        return this.orderService.fetchAll();
    }

    @GetMapping("/{id}")
    public Order fetchById(@PathVariable long id){
        return this.orderService.fetchById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id){
        this.orderService.deleteById(id);
    }

}
