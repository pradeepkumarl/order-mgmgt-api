package com.classpath.ordermgmt.controller;

import com.classpath.ordermgmt.dao.CustomerRepository;
import com.classpath.ordermgmt.model.Customer;
import com.classpath.ordermgmt.model.Order;
import com.classpath.ordermgmt.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/customers")
@AllArgsConstructor
public class CustomerController {

    private CustomerService customerService;

    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer){
        //System.out.println(customer.getOrders().get(0).getOrderLineItems());
        customer
                .getOrders()
                .forEach(order -> {
                    order.setCustomer(customer);
          //          order.getOrderLineItems()
            //                .forEach(orderLineItem -> orderLineItem.setOrder(order));

        });
        return this.customerService.save(customer);
    }

    @GetMapping("/{custId}/orders")
    public Set<Order> fetchOrdersByCustomerId(@PathVariable("custId") long customerId){
     return  this.customerService.fetchOrdersByCustomerId(customerId);
    }

    @GetMapping
    public Set<Customer> fetchCustomers(){
     return  this.customerService.fetchAllCustomers();
    }
    @GetMapping("/{custId}")
    public Customer fetchCustomerById(@PathVariable("custId") long customerId){
     return this.customerService.fetchCustomerById(customerId);
    }

    @PutMapping("/{custId}/orders")
    public Set<Order> updateOrdersByCustomerId(@PathVariable("custId") long customerId, @RequestBody @Valid List<Order> orders){
        return  this.customerService.updateOrdersByCustomerId(customerId, orders);
    }

}