package com.classpath.ordermgmt.service;

import com.classpath.ordermgmt.dao.CustomerRepository;
import com.classpath.ordermgmt.dao.OrderRepository;
import com.classpath.ordermgmt.model.Customer;
import com.classpath.ordermgmt.model.Order;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    private OrderRepository orderRepository;

    @Override
    public Set<Order> fetchOrdersByCustomerId(long customerId) {
        Customer customer = findCustomerById(customerId);
        return this.orderRepository.findOrdersByCustomer(customer);
    }

    @Override
    public Customer save(Customer customer) {
        return this.customerRepository.save(customer);
    }

    @Override
    public Set<Customer> fetchAllCustomers() {
        return new HashSet<>(this.customerRepository.findAll());
    }

    @Override
    public Set<Order> updateOrdersByCustomerId(long customerId, List<Order> orders) {
        Customer customer = findCustomerById(customerId);
        orders.stream().forEach(customer::createOrder);
        this.customerRepository.save(customer);
        return new HashSet<>(customer.getOrders());
    }

    @Override
    public Customer fetchCustomerById(long customerId) {
        return this.customerRepository.findById(customerId).orElseThrow(() -> new IllegalArgumentException("customer not present"));
    }

    private Customer findCustomerById(long customerId){
        return  this.customerRepository
                .findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not present for the passed id"));

    }
}