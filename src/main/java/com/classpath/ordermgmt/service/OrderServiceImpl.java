package com.classpath.ordermgmt.service;

import com.classpath.ordermgmt.dao.OrderRepository;
import com.classpath.ordermgmt.model.Order;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        log.info("Inside the save method of OrderService implementation :: ");
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchAll() {
        log.info("Inside the fetchAll method of OrderService implementation :: ");
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchById(long id) {
        log.info("Inside the fetchById method of OrderService implementation :: ");
        return this.orderRepository.findById(id)
                                        .orElseThrow(()-> new IllegalArgumentException("Invalid order id"));
    }

    @Override
    public void deleteById(long id) {
        log.info("Inside the deleteById method of OrderService implementation :: ");
        this.orderRepository.deleteById(id);
    }
}