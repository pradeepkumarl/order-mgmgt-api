package com.classpath.ordermgmt.service;

import com.classpath.ordermgmt.model.Customer;
import com.classpath.ordermgmt.model.Order;

import java.util.List;
import java.util.Set;

public interface CustomerService {
    
    Set<Order> fetchOrdersByCustomerId(long customerId);

    Customer save(Customer customer);

    Set<Customer> fetchAllCustomers();

    Set<Order> updateOrdersByCustomerId(long customerId, List<Order> orders);

    Customer fetchCustomerById(long customerId);
}