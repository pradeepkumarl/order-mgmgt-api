package com.classpath.ordermgmt.service;

import com.classpath.ordermgmt.model.Order;

import java.util.Optional;
import java.util.Set;

public interface OrderService {

    Order save(Order order);

    Set<Order> fetchAll();

    Order fetchById(long id);

    void deleteById(long id);
}