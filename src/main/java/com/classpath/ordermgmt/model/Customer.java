package com.classpath.ordermgmt.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String email;

    private String phone;

    @OneToMany(mappedBy = "customer", fetch = EAGER, cascade = ALL)
    private List<Order> orders;

    public void createOrder(Order order){
        this.orders.add(order);
        order.setCustomer(this);
    }
}