package com.classpath.ordermgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Setter
@Getter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Column(name="created_date", nullable = false, updatable = false)
    @NotNull(message = "Created on field cannot be null")
    private LocalDate createdOn;

    @Column(name="price", nullable = false, updatable = false)
    @Min(value = 15000, message = "Price cannot be less than 15k")
    @Max(value = 50000, message = "Price cannot be more than 50k")
    private double price;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    @Setter
    private Customer customer;

   /* @OneToMany(mappedBy = "order",
               cascade = ALL,
               fetch = EAGER)
    private Set<OrderLineItem> orderLineItems;

    */
}