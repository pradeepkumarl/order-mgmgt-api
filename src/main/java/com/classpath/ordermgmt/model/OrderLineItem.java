package com.classpath.ordermgmt.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="order_line_items")
@Setter
@Getter
@ToString(exclude = "order")
@EqualsAndHashCode(of = {"id", "qty"})
public class OrderLineItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    private int id;

    @Column(name="qty")
    private int qty;

    @Column(name="unit_price")
    private double unitPrice;

    @Column(name="item_name")
    private String name;

   /* @ManyToOne
    @JoinColumn(name="orders", nullable = false)
    private Order order;
    */
}