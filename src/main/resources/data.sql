insert into user (user_id, email_address, password, username) values (1, 'kiran@gmail.com', '$2a$10$8gnyd/8sRzGfqs9sBEcpk.3mA/qlNI7rM7QhT/AmkdT8ps56Jip5u', 'kiran');
insert into user (user_id, email_address, password, username) values (2, 'vinay@gmail.com', '$2a$10$8gnyd/8sRzGfqs9sBEcpk.3mA/qlNI7rM7QhT/AmkdT8ps56Jip5u', 'vinay');

insert into role (role_id, role_name) values (1, 'ROLE_USER');
insert into role (role_id, role_name) values (2, 'ROLE_ADMIN');

insert into user_roles(user_id, role_id) values (1, 1),(2, 1), (2,2);
